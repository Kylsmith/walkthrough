var Walkthrough = {
	content: $('#content'),
	user: {},
	pageTitle: {
		all: 'TS Walkthrough',
		separator: ': ',
		login: 'Login',
		home: 'Home',
	},
	toastType: {
		warn: 'i-warn',
		person: 'i-street-view',
		question: 'i-question',
		info: 'i-info',
		
	},
	
	clickedWalkThrough:{},
	
	unsavedEdits: false,
	
	init: function() {
		System.init([
			{name: 'users', file: 'db/users.json', preload: true },	
		]);
		this.bindRoutes();
	},
	
	bindRoutes: function() {
		System.route.add('/', 'login', this, this.page.login);
		System.route.add('/home', 'home', this, this.page.home);
		System.route.add('/createwalkthrough','createwalkthrough', this, this.page.createwalkthrough);
		System.route.add('/selectedwalkthrough','selectedwalkthrough', this, this.page.selectedwalkthrough);
		System.route.add('/logout', 'logout', this, this.page.logout);
		System.route.listen();
	},
	
	isLoggedIn: function() {
		// Checks localStorage to see if a user has already logged in
		var info = localStorage.getItem('WalkthroughUser');
		if (info) {
			this.user = JSON.parse(info);
			this.user['signature'] = this.user.user + System.util.createHash(info);
			return info;
		}
		return false;
	},
	
	setPageTitle: function(page) {
		// Build the page title based on the page currently being viewed
		if (this.pageTitle[page]) {
			document.title = this.pageTitle.all + this.pageTitle.separator + this.pageTitle[page];
		} else {
			document.title = this.pageTitle.all;
		}
	},
	
		
	switchContent: function(page, options) {
		// Animates the content switching mechanism
		var content = $('#page_'+page).html();
		this.setPageTitle(page);
		Walkthrough.content.fadeOut('fast', function() {
			Walkthrough.content.html(content);
			Walkthrough.content.fadeIn('fast');
			if (Walkthrough.bind[page]) Walkthrough.bind[page](options);
		});
	},
	
	deleteWalkThrough: function(name) {
		System.modal('#confirmDeleteModal', function() {
			var prompt = 'Are you sure you want to delete this WalkThrough?'
			$('#confirmDeleteModal').find('span').html(prompt);
			
			$('#confirmDelete').on('click', function(e) {
				$.ajax({
					url: 'php/deletewalkthrough.php',
					data: {'data' : name },
					success: function (response) {
						System.route.move('/home');
					}	
				});
			});
		});
		
	},
	
	initializeWalkThrough: function(clickedWalkThrough) {
		var app = Walkthrough;
		var retreivedObject = localStorage.getItem('selectedWalkThrough');
		var clickedWT = JSON.parse(retreivedObject);
		System.route.move('/selectedwalkthrough');
		$('#title').empty();
		$('#steps').empty();
		$('#title').append(clickedWalkThrough['name']);
		if (Walkthrough.user['user'] === clickedWT['createdBy']) {
			$.each(clickedWalkThrough['steps'], function(i) {
				$('#steps').append('<li><input type="checkbox">\
				    <span class="display" >' + clickedWalkThrough['steps'][i] +' </span>\
				    <textarea class="selectedWalkThroughBtn edit" name="walkthroughstep"  style="display:none"/>\
				    <button  type="editStep" class="selectedWalkThroughBtn editStep" >Edit</button>\
				    <button hidden type="deleteStep" class="selectedWalkThroughBtn deleteButton" id="Delete">Delete</button>\
			  	</li>');
			});
		} else {
			$('#editBtns').hide();
			$.each(clickedWalkThrough['steps'], function(i) {
				$('#steps').append('<li><input type="checkbox">\
				    <span class="display" >' + clickedWalkThrough['steps'][i] +' </span>\
				    <textarea class="selectedWalkThroughBtn edit" name="walkthroughstep"  style="display:none"/>\
			  	</li>');
			});
		};
	},
	
	submitEdit: function(){
		var app = Walkthrough;
		$('#removeStep').hide();
			var walkThroughObj = {
				createdBy:Walkthrough.user['user'],
				name: app.clickedWalkThrough['name'],
				steps: [],
			}
			console.log(walkThroughObj);
			
			$('.display').each(function (event) {
				var step = app.noHtml($(this).text().trim());
				if(step.length > 0) {
					walkThroughObj.steps.push(step);
				}
			});
			
			$('.toSubmit').each(function (event) {
				var step = app.noHtml($(this).val().trim());
				if(step.length > 0) {
					walkThroughObj.steps.push(step);
				}
			});
			
			System.file.save( 'db/walkthroughs/' + app.clickedWalkThrough['name'] + '.json', walkThroughObj);
			app.initializeWalkThrough(walkThroughObj);
			$('#submitEdit').html('Edits Saved');
			$('#submitEdit').prop('disabled', true);
			$('#addStepDiv').empty().hide();
	},
	
	toast: function(text, type) {
		if (!this.toastType[type]) type = 'info';
		$('<div>', {
			class: (type) ? 'toast '+this.toastType[type] : 'toast',
			html: text
		}).prependTo('#toastBox').on('click', function() {
			this.remove();
		}).delay(3000).animate({
			opacity: 0.0
		}, {
			duration: 1000,
			done: function() {
				this.remove();
			}
		});
	},
	
	noHtml: function(str) {
    	return String(str).replace(/(<([^>]+)>)/ig, "");
  	},
  	
  // 	sanitizeTitle: function(str){
  // 		var rg1=/^[^\\/:\*\?"<>\|]+$/; // forbidden characters \ / : * ? " < > |
		// var rg2=/^\./; // cannot start with dot (.)
		// var rg3=/^(nul|prn|con|lpt[0-9]|com[0-9])(\.|$)/i; // forbidden file names
		// str = str.replace(rg1,'');
		// str = str.replace(rg2,'');
		// str = str.replace(rg3,'');
		// return str;
		// console.log(str)
  // 	},
	
	page: {
		login: function(options) {
			if (Walkthrough.isLoggedIn()) {
				System.route.move('/home');
			}else{
				Walkthrough.switchContent('login', options);
			};
		},
		
		logout: function(options) {
			if (this.isLoggedIn()) {
				localStorage.removeItem('WalkthroughUser');
				Walkthrough.user = {};
			}
			System.route.move('/');
			this.toast('Logged out', 'person');
		},
		
		home: function(options) {
			var app = Walkthrough;
			if (!Walkthrough.isLoggedIn()) {
				System.route.move('/');
			} else {
				if (app.unsavedEdits) {
					System.modal('#unsavedNotice', function() {
						var alert = 'You have unsaved edits to this walkthrough. Leaving this page will cause you to lose them. Proceed?'
						$('#unsavedNotice').find('span').html(alert);
						
						$('#loseEdits').on('click', function(e) {
							Walkthrough.switchContent('home', options);
						});
					});
				} else {
					Walkthrough.switchContent('home', options);
				}
			};
		},
		
		selectedwalkthrough: function(options) {
			if (!Walkthrough.isLoggedIn()) {
				System.route.move('/');
			} else {
				Walkthrough.switchContent('selectedwalkthrough', options);
			};
		},
		
		createwalkthrough: function(options) {
			if (!Walkthrough.isLoggedIn()) {
				System.route.move('/');
			} else {
				Walkthrough.switchContent('createwalkthrough', options);
			};
		},
	},
	
	bind: {
		login: function(options) {
			$('#loginName, #loginPass, #loginButton').off('click keyup').on('click keyup', function loginAttempt(event) {
				if ((event.target.id === 'loginButton' && event.type === 'click') || event.keyCode === 13) {
					var name = $('#loginName').val();
					var pass = $('#loginPass').val();
					if (name && pass) {
						System.ldap.login($('#loginName').val(), $('#loginPass').val()).done(function loginAttempt(result) {
							if (result !== 'false') {
								localStorage.setItem('WalkthroughUser', result);
								Walkthrough.user = JSON.parse(result);
								Walkthrough.toast('Logged in as ' + Walkthrough.user.user, 'person');
								System.route.move('/home');
							} else {
								System.log('Invalid username or password', 'local');
								Walkthrough.toast('Invalid username or password', 'warn');
							}
						});
					} else {
						System.log('Please supply a username and password.', 'local');
						Walkthrough.toast('Please supply a username and a password', 'warn');
					}
				}
			});	
		}, 
		
		home: function(options) {
			var app = Walkthrough;
			var table = $('#walkthrough');
			$.get('php/listwalkthroughs.php', function (result) {
				var data = JSON.parse(result);
				console.log(data);
				table.dataTable({
				    "aaData": data,
				        "aoColumns": [{
				        "mDataProp": "createdBy"
				    }, {
				        "mDataProp": "name"
				    }, {
				        "mDataProp": "steps"
				    }],
						'columnDefs': [
							{
								'targets': [2],
								'visible': false,
								'searchable': false
							}
						]
				});
				
				table.find('tr').on('dblclick', function (event) {
					app.clickedWalkThrough = $('#walkthrough').DataTable().row($(event.target).parents('tr')).data();
					localStorage.setItem( 'selectedWalkThrough', JSON.stringify(app.clickedWalkThrough) );
					System.route.move('/selectedwalkthrough');
				});				 
			});
		},
		
		createwalkthrough: function(options) {
			var app = Walkthrough;
			var isValid = (function(){
				var rg1=/^[^\\/:\*\?"<>\|]+$/; // forbidden characters \ / : * ? " < > |
				var rg2=/^\./; // cannot start with dot (.)
				var rg3=/^(nul|prn|con|lpt[0-9]|com[0-9])(\.|$)/i; // forbidden file names
				return function isValid(fname){
					return rg1.test(fname)&&!rg2.test(fname)&&!rg3.test(fname);
				}
			})();
			
			$('#walkThroughSubmit').off('click').on('click', function (event) {
				var	title = $('[name="walkthroughtitle"]').val();
				if (title.length > 0)  {
					var	step = app.noHtml($('#Walkthroughstep').val());
					var walkThroughObj = {
						createdBy:Walkthrough.user['user'],
						name: title,
						steps: [],
					}
					
					$('[name="walkthroughstep"]').each(function (event) {
						if($(this).val()) {
							walkThroughObj.steps.push(app.noHtml($(this).val()));
						}
					});
					 	$.ajax({
					      	url: 'php/doesfileexist.php',
					      	data: {'data' : $('[name="walkthroughtitle"]').val() + '.json'},
					      	success: function (result) {
					         	if(result === 'true') {
					         		System.modal('#errorModal', function errorModal() {
												var errorMsg = 'This walkthrough name has already been taken'
												$('#errorModal').find('span').html(errorMsg);
											});
					         	} else if ( isValid(title) === true ){
					         		console.log('The-party-is-lit-boi.');
											System.file.save( 'db/walkthroughs/' + title + '.json', walkThroughObj);
											System.route.move('/home');
					         	} else {
											System.modal('#errorModal', function errorModal() {
												var errorMsg = 'Please use valid file characters'
												$('#errorModal').find('span').html(errorMsg);
											});
					         	}
					      	}
					    });
				} else {
					System.modal('#errorModal', function errorModal() {
						var errorMsg = 'Please name your walkthrough'
						$('#errorModal').find('span').html(errorMsg);
					});
				}
			});
			
			var step = $('#addStepDiv').html();
			$('#addStep').off('click').on('click', function (event) {
				$('#addStepDiv').append(step);	
			});
		},
		
		selectedwalkthrough:function(options) {
			var app = Walkthrough;
			var retreivedObject = localStorage.getItem('selectedWalkThrough');
			var clickedWT = JSON.parse(retreivedObject);
			app.initializeWalkThrough(clickedWT);
			$('#addStepDiv').hide();
			$('#removeStep').hide();
			$('.deleteButton').hide();
			
			$(".editStep").click(function(){
				$('#submitEdit').removeClass('hidden');
				$('#submitEdit').html('Save Edits');
				app.unsavedEdits = true;
				$(this).siblings('.display').hide();
				$(this).siblings('.deleteButton').toggle();
				$(this).text(function(i,text){
					return text === 'Save' ? 'Edit' : 'Save';
				});
				if ($(this).siblings('.edit').is(':visible')) {
					$(this).siblings(".display").empty().show().append(app.noHtml($(this).siblings('.edit').eq(0).val() ));
					$(this).siblings('.edit').hide();
					console.log(app.noHtml($(this).siblings('.edit').eq(0).val()) );
				} else {
					$(this).siblings(".edit").show().val(app.noHtml($(this).siblings('.display').text()) ).focus();
					console.log(app.noHtml($(this).siblings('.display').text()));
				};
				
				$('.deleteButton').off('click').on('click', function(event) {
					var step = $(this)
					System.modal('#confirmRemoveStep', function() {
						var prompt = 'Are you sure you want to remove this step?';
						$('#confirmRemoveStep').find('span').html(prompt);
						$('#confirmRemove').on('click', function(e) {
							step.parent().remove();
						});
					});					
				});
			});
						
			var step = $('#addStepDiv').html();
			$('#addStep').off('click').on('click', function (event) {
				$('#removeStep').show();
				$('#submitEdit').removeClass('hidden');
				$('#submitEdit').html('Save Edits');
				if ( $('#addStepDiv').is(':hidden') ) {
					$('#addStepDiv').show();
				} else {
					$('#addStepDiv').append(step);
				};
			});
			
			$('#removeStep').off('click').on('click', function (event) {
				$('#addStepDiv').children().last().detach();
				if ( $('#addStepDiv').children().length == 0 ) {
				  $('#removeStep').hide();
				}	
			});
						
			$('#submitEdit').off('click').on('click', function (event) {
				app.unsavedEdits = false;
				Walkthrough.submitEdit();
			});
			
			$('#deleteWalkThrough').off('click').on('click', function (event) {
				var wtToDelete = app.clickedWalkThrough['name'] + '.json';
				app.deleteWalkThrough(wtToDelete);				
			});
		},
		
		
		profile: function(options) {
			console.log(options);
		},
		
	},
}

Walkthrough.init();
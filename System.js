var System = {
	_db: {},
	files: {},
	ready: false,
	loggedIn: [ false, false ],
	cookieExpire: 14,
	routes: {},
	init: function init(files) {
		// Initializes System with a list of file objects
		//	[
		//		{ name: '', file: '', preload: false },
		//	]
		// if (files.length > 0) {
			// var promises = [];
			// $.each(files, function loadFiles(index, item) {
				// System.files[item.name] = item;
				// if (item.preload) promises.push(System.file.load(item.name));
			// });
			
			// return $.when.apply($, promises).done(function filesLoaded() {
				// System.ready = true;
				// System.log(Object.keys(System._db).length + ' JSON files loaded', 'local');
			// });
		// } else {
			System.ready = true;
			// System.log('No files loaded', 'local');
		// }
	},
	
	log: function log(message, type) {
		var caller = System.log.caller.name || '';
		if (!type) type = 'local';
		if (type === 'local') {
			var now = new Date().toISOString();
			console.log(now, caller + '()', message);
		} else {
			$.ajax({
				url: 'php/log.php',
				data: {
					m: message,
					l: type,
					s: caller + '()'
				}
			});
		}
	},	
	
	ldap: {
		login: function(username, password) {
			return $.ajax({
				url: 'php/ldap.php',
				method: 'POST',
				data: {
					user: username,
					password: password
				}
			});
		},
		logout: function() {},
		
	},
	
	cookie: {
		bake: function cookieBake(name, value, expires) {
			// Bakes a new cookie. If no expires date is provided, the cookie will become a session cookie.
			var cookie = name + '=' + encodeURI(value) + ';';

			if (expires) {
				expires = new Date(expires);
				cookie += 'expires=' + expires.toUTCString() + ';';
			}
			document.cookie = cookie;
		},

		get: function cookieGet(name) {
			// Searches document.cookie for a cookie by a given name and returns the name+value if one is found.
			var brokenCookies = document.cookie.split(/;\s*/);
			for(var i = 0; i < brokenCookies.length; i++) {
				var foundName = brokenCookies[i].split('=')[0];
				var foundValue = brokenCookies[i].split('=')[1];
				if (foundName === name) return foundValue;
			}
			return false;
		},

		eat: function cookieEat(name) {
			// Deletes a given cookie by setting an expiration date from the past
			if (System.cookie.get(name)) {
				var date = new Date(1985, 3, 22);
				System.cookie.bake(name, '', date);
			}
		},
	
		exists: function cookieExists() {
			var cookie = [
				System.cookie.get('ohmanitstheloggedinusername'),
				System.cookie.get('andherestheirpasswordhash')
			];
			if (cookie[0] && cookie[1]) return true;
			return false;
		},
	},
	
	file: {
		load: function fileLoad(file) {
            // Load a json file into a variable
            // Returns the data as a javascript object or false
            return $.ajax({
                url: 'php/file.php',
                method: 'POST',
                data: {
                    f: file
                }
            }).then(function fileLoaded(result) {
                if (result !== 'false') {
                    //System._db[name] = JSON.parse(result);
                    try {
                        result = JSON.parse(result);
                    } catch(error) {
                    	console.log(error)
                    }
                    return result;
                } else {
                    System.log(file + ' could not be loaded', 'error');
                    return false;
                }
            });
        },
		
		save: function fileSave(file, contents) {
			// Save a file
			// Returns true if it succeeded or false if it didn't
			return $.ajax({
				url: 'php/file.php',
				method: 'POST',
				data: {
					f: file,
					d: JSON.stringify(contents)
				}
			}).then(function fileSaved(result) {
				console.log(file,JSON.stringify(contents),result);
				if (result === 'false' || result === null) {
					System.log('Error writing to ' + System.files[name].file, 'error');
					return false;
				}
				return true;
			});
		},
	},

	route: {
		add: function routeAdd(path, name, parent, handler) {
			System.routes[path] = {
				'name': name, 'path': path, 'handler': handler, 'parent': parent
			};
		},
		
		remove: function routeRemove(path) {
			delete System.routes[path];
		},
		
		listen: function routeListen() {
			if (Object.keys(System.routes).length > 0) {
				var handler = function hashChange(event) {
					var path = location.hash.slice(1) || '/';
					var levels = path.split('/').length;
					var split = path.split('/');
					split.shift();
					
					if (System.routes[path]) {
						var parent = System.routes[path].parent;
						System.routes[path].handler.call(parent, path.split('/').slice(levels));
					} else {
						var temp = path.split('/');
						for (var i = 1; i < levels; i++) {
							if (System.routes[temp.join('/')]) {
								path = temp.join('/');
								var parent = System.routes[path].parent;
								System.routes[path].handler.call(parent, split.slice(levels - i));
								break;
							}
							temp.pop();
						}
					}
				};
				window.onhashchange = handler;
				window.onload = handler;
				System.log('System is now listening for hashchanges on ' + Object.keys(System.routes).length + ' route(s)', 'local');
			} else {
				System.log('Cannot listen to route changes - no routes have been defined.', 'local');
			}
		},
	
		move: function routeMove(path) {
			if (System.routes[path]) {
				window.location.href = window.location.href.replace(/#(.*)$/, '') + '#' + path
			}
		},
	},

	util: {
		toTitleCase: function toTitleCase(string) {
			// Converts a string to proper case: String, Addama, Power
			return string.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
		},

		varDump: function varDump(data) {
			// Returns a nicely indented string version of the given variable for logging/debugging purposes
			return JSON.stringify(data, null, '\t');
		},
		
		htmlSpecialChars: function htmlSpecialChars(unsafe) {
			// Sanitizes a string for use in XML
			if (typeof unsafe === 'string') return unsafe.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;");
			return unsafe;
		},
			
		makeRandomString: function makeRandomString(length, bits) {
			// Creates a randomized string of the given length, in the given bits
			// Useful for creating generic but unique IDs, session keys, or whatever
			length = length || 20;
			bits = bits || 36;
			var result = "";
			var temp;
			while (result.length < length) {
				temp = Math.random().toString(bits).slice(2);
				result += temp.slice(0, Math.min(temp.length, (length - result.length)));
			}
			return result.toUpperCase();
		},
	
		fromConsole: function fromConsole() {
			var stack;
			try {
			   throw new Error();
			} catch (e) {
				stack = e.stack;
			}
			
			if (!stack) return false;
			var lines = stack.split("\n");
			for (var i = 0; i < lines.length; i++) {
				// Chrome console
				if (lines[i].indexOf("at Object.InjectedScript.") >= 0) return true;  
				// Firefox console
				if (lines[i].indexOf("@debugger eval code") == 0) return true; 
				// Safari console				
				if (lines[i].indexOf("_evaluateOn") == 0) return true;   
			}
			return false;
		},
	
		createHash: function createHash(string) {
			// Used to create hashes of strings to verify their contents
			var hash = 0, i, chr;
			if (string.length == 0) return hash;
			for (i = 0; i < string.length; i++) {
				chr   = string.charCodeAt(i);
				hash  = ((hash << 5) - hash) + chr;
				hash |= 0; // Convert to 32bit integer
			}
			return hash;
		},
		
		getDate: function(date) {
			var today = (date) ? new Date(date) : new Date();
			if (today.toJSON()) {
				return {
					'day': today.getDate(), 
					'month': today.getMonth() + 1, 
					'year': today.getFullYear(),
					'hour': today.getHours(),
					'minute': today.getMinutes(),
					'second': today.getSeconds(),
					'full': today.toJSON(),
					'unix': today.getTime()
				};
			} 
			return false;
		},
		
		convertMillisToDays: function convertMillisToDays(millis) {
			var day = 86400000;
			if (millis < (day / 2)) return 0;
			return millis / day;
		},
		
		convertDaysToMillis: function convertDaysToMillis(days) {
			return days * 86400000;
		},
	},
	
	modal: function modalToggle(targetID, handler) {
		// Expects modals to have the following minimal architecture:
    // div.modalWrapper#whateverTheIDIs <-- display: none
		//         div.modalContent
		//             button.modalClose
		//             Your content here
		// The handler should bind all the buttons and content inside the modal,
		// as the only thing pre-bound (but changeable) is the close button
		var modal = $(targetID);
		if (modal) {
			if (modal.css('display') === 'none') {
					// Show the modal
					modal.css('display', 'block');
					// Bind the close button if there is one
					modal.find('.modalClose').off('click').on('click', function() {
							// Hide the modal
							modal.css('display', 'none');
					});
					// Run the setup function if there is one
					if (handler && typeof handler === 'function') {
							handler.call(modal);
					}
			} else {
					// Hide modals that are already displayed
					modal.css('display', 'none');
			}
		}
	},
}
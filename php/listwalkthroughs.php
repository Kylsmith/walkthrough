<?php

$directory = '../db/walkthroughs';
$scanned_directory = array_diff(scandir($directory), array('..', '.'));
$data = array();
foreach ($scanned_directory as  $value) {
    // echo json_encode( $directory . '/' . $value);
    $contents = file_get_contents('../db/walkthroughs/' . $value);
    $data[] = json_decode($contents);
}

echo json_encode($data);

?>
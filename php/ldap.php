<?php
	// Return a JSON object if the user is good, or 'false' if not
	if (isset($_POST['user']) && isset($_POST['password'])) {
		// CHANGE THESE
		$server = '10.1.10.18';		// Primary Domain Controller
		$domain = 'tiger-sheep';
		$baseDN = 'DC=tsusers,DC=tiger-sheep,DC=com';	// Exploded domain
		
		// Connect to the LDAP server
		$ldap = ldap_connect($server) or die('Could not connect to LDAP server');
		$username = $_POST['user'];
		ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
		ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);
		@ldap_bind($ldap, $username . '@' . $domain, $_POST['password']) or die('false');

		// We will return the info as a JSON object
		$return = array(
			'user' => $_POST['user'],
			'name' => array(),
			'groups' => array()
		);
		
		// Find the record for the user
		$filter = "(samaccountname=$username)";
		$resource = ldap_search($ldap, $baseDN, $filter);
		$data = ldap_get_entries($ldap, $resource);
		
		// Split out the Names. Alternatively, there is the 'cn' attribute 
		// which is both names combined
		$return['name'][] = $data[0]['givenname'][0];
		$return['name'][] = $data[0]['sn'][0];
		
		// Split out the CNs for the user's groups
		foreach($data[0]['memberof'] as $key => $value) {
			if ($key === 'count') continue;
			$split = explode(',', $value);
			$return['groups'][] = explode('=', $split[0])[1];
		}
		
		@ldap_close($ldap);
		echo json_encode($return);
	} else {
		echo 'false';
	}
?>
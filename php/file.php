<?php
	if (isset($_POST['f'])) {
		if (isset($_POST['d'])) {
			save($_POST['f'], $_POST['d']);
		} else {
			load($_POST['f']);
		}
	}

	function save($file, $data) {
		// Make any directories that don't exist
		$parts = explode('/', $file);
		$name = array_pop($parts);
		$dir = '../';
		foreach($parts as $part) {
			if (!is_dir($dir . "/$part")) mkdir($dir);
		}
		echo file_put_contents('../' . $file, $data);
		chmod('../' . $file, 0777);
	}
	
	function load($file) {
		if (file_exists('../' . $file)) {
			echo file_get_contents('../' . $file);
		} else {
			echo 'false';
		}
	}
	
?>
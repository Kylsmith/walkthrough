<!DOCTYPE html>
<html>

	<head>
		<title></title>
		<link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Roboto:400,700,100,300'>
		<link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Inconsolata:400,700'>
		<link rel='stylesheet' type='text/css' href='css/bootstrap.min.css'>
		<link rel='stylesheet' type='text/css' href='css/dataTables.bootstrap.min.css'>
		<link rel='stylesheet' type='text/css' href='css/icons.css'>
		<link rel='stylesheet' type='text/css' href='css/walkthroughstyle.css' />
	</head>
	
	<body>
	
		<div id='toastBox'></div>
		<div id='content'></div>
		
		<script type='text/html' id='page_login'>
			<?php require_once('inc/login.php'); ?>
		</script>
		
		<script type='text/html' id='page_home'>
			<div class='wrapper'>
				<?php require_once('inc/home.php'); ?>
			</div>
		</script>
		
		<script type="text/html" id='page_createwalkthrough'>
			<div class='wrapper'>
				<?php require_once('inc/createwalkthrough.php'); ?>
			</div>
		</script>
		
		<script type="text/html" id='page_selectedwalkthrough'>
			<div class='wrapper'>
				<?php require_once('inc/selectedwalkthrough.php'); ?>
			</div>
		</script>

		<script type='text/javascript' src='jquery-2.1.4.min.js'></script>
		<script type='text/javascript' src='jquery.dataTables.min.js'></script>
		<script type='text/javascript' src='bootstrap.min.js'></script>
		<script type='text/javascript' src='dataTables.bootstrap.min.js'></script>
		<script type='text/javascript' src='System.js'></script>
		<script type='text/javascript' src='Walkthrough.js'></script>
	</body>
</html>
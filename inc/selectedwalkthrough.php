	<?php require('inc/menu.php'); ?>
	<div id='selectedWalkThrough' >
		<h2 id="title"></h2>
			<ol id="steps">
				
			</ol>
		<div  id="addStepDiv">
			<textarea  class="toSubmit " rows="4" cols="25" placeholder='Step' > </textarea>
		</div>
		<div id='editBtns'>
			<button class='selectedWalkThroughBtn i-folder-open hidden' id="submitEdit">Save Edits</button>
			<button class='selectedWalkThroughBtn i-plus' type="add"  id='addStep' >Add Step</button>
			<button class='selectedWalkThroughBtn i-mist' type="add"  id='removeStep' >Undo Add Step</button>
			<button  class='selectedWalkThroughBtn i-fire' id='deleteWalkThrough'>Delete Walkthrough</button>
		</div>
		
		<div id="confirmDeleteModal" class="modalWrapper">
			<div class="modalContent">
				<span class="prompt"></span>
				<button id="confirmDelete" class="modalClose">Yes</button>
				<button class="modalClose">No</button>
			</div>
		</div>
		
		<div id="confirmRemoveStep" class="modalWrapper">
			<div class="modalContent">
				<span class="prompt"></span>
				<button id="confirmRemove" class="modalClose">Yes</button>
				<button class="modalClose">No</button>
			</div>
		</div>
		
		<div id="unsavedNotice" class="modalWrapper">
			<div class="modalContent">
				<span class="prompt"></span>
				<button id="loseEdits" class="modalClose">Yes</button>
				<button class="modalClose">No</button>
			</div>
		</div>
	</div>



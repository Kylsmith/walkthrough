	<?php require('inc/menu.php'); ?>
	<div id='createPanel' >
	<h2>Create New Walkthrough</h2>
	<div class="form-group">
		<label for="walkthroughTitle">Title</label>
		<input type='text' class="form-control" placeholder='Title' name="walkthroughtitle" />
	</div>
		<br>
		<br>
		<div class="form-group">
			<label for="walkthroughsteps">Steps</label>
			<div id="addStepDiv">
				<br>
				<textarea  rows="4" cols="25" class="form-control" placeholder='Step' name="walkthroughstep"> </textarea>
			</div>
		</div>
		<div class="form-group">
			<button type="add"  id='addStep'>Add Step</button>
			<button type="submit"  id='walkThroughSubmit' value="Submit">Submit Walkthrough</button>
		</div>
		<div id="errorModal" class="modalWrapper">
			<div class="modalContent">
				<button class="modalClose">OK</button>
				<span id="errorMsg"></span>
			</div>
		</div>
	</div>
